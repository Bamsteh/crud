package src.app.utils;

import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.exceptions.WrongPathException;
import src.app.utils.command.Extension;

import java.io.*;

public class FileUtils {

    public static File createFile(String fileName, Extension extension){
        String path = Paths.definePath(fileName, extension);
        File newFile = new File(path);
        try {
            if (newFile.createNewFile()) {
                return newFile;
            }
        } catch (IOException err) {
            throw new WrongPathException("File path is incorrect");
        }
        return newFile;
    }

    public static boolean writeToFile(String fileName, Extension extension, String content){
        if(content == null){
            return false;
        }
        String path = Paths.definePath(fileName, extension);
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(path, false))){
            writer.write(content);
            return true;
        }
        catch (IOException err){
            throw new WrongPathException("File not found");
        }
    }

    public static String readFile(String fileName, Extension extension) {
        String path = Paths.definePath(fileName, extension);
        StringBuilder fileContent = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!fileContent.toString().equals("")) {
                    fileContent.append("\r\n");
                }
                fileContent.append(line);
            }
            return fileContent.toString();
        } catch (IOException err) {
            throw new WrongPathException("Path is incorrect");
        }
    }

    public static String readFile(String filePath) {
        StringBuilder fileContent = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!fileContent.toString().equals("")) {
                    fileContent.append("\r\n");
                }
                fileContent.append(line);
            }
            return fileContent.toString();
        } catch (IOException err) {
            throw new WrongPathException("Path is incorrect");
        }
    }


    public static boolean deleteFile(String fileName, Extension extension) {
        String path = Paths.definePath(fileName, extension);
        File file = new File(path);
        if (file.exists() && file.isFile()) {
            return file.delete();
        } else {
            return false;
        }
    }


    public static byte[] readBinFile(String fileName) {
        String pathToFile = Paths.getBinBasePath() +
                fileName;
        File file = new File(pathToFile);
        if(!file.exists()){
            throw new WrongPathException("File not exists");
        }
        FileInputStream fis;
        byte[] data = new byte[(int) file.length()];
        try{
            fis = new FileInputStream(file);
            if(fis.read(data) == -1){
                throw new WrongFormatException("Failed to read file");
            }
            fis.close();
            return data;
        } catch (IOException e){
            throw new WrongPathException("Failed to read file");
        }
    }


    public static void writeToBinFile(byte[] data, String fileName) {
        String pathToFile = Paths.getBinBasePath() + fileName;
        File file = new File(pathToFile);
        if(!file.exists()){
            throw new WrongPathException("File not exists", new IllegalArgumentException());
        }
        FileOutputStream fos;
        try{
            fos = new FileOutputStream(file);
            fos.write(data);
            fos.close();
        } catch (IOException e){
            throw new WrongPathException("Failed to write file");
        }
    }
}