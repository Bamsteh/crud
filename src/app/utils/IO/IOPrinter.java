package src.app.utils.IO;

public class IOPrinter {
    public static void printLine(String message){
        System.out.println(message);
    }

    public static void printLine(){
        System.out.println("\n");
    }

    public static void print(String message){
        System.out.print(message);
    }
}
