package src.app.utils.IO;

import java.io.ByteArrayInputStream;
import java.util.Scanner;

public class IOScanner {

    private static Scanner scanner = new Scanner(System.in);

    public static String readLine(){
        return scanner.nextLine();
    }

    //used only for tests
    public static void setInputStream(ByteArrayInputStream inputStream){
        scanner = new Scanner(inputStream);
    }

    //used only for tests
    public static void setDefaultInputStream(){
        scanner = new Scanner(System.in);
    }
}
