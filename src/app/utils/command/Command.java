package src.app.utils.command;

import src.app.utils.PatternMatcher;
import src.app.utils.exceptions.WrongFormatException;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Command {

    private CommandType command;
    private Long id;
    private String fileName;
    private Extension extension;

    public Command(String input) throws WrongFormatException {

        if (input == null || input.isBlank()) {
            throw new WrongFormatException("Null argument.");
        }

        String fileName = PatternMatcher.takeFileName(input);
        String command = PatternMatcher.takeCommand(input);
        String id = PatternMatcher.takeId(input);

        if(!isValidInput(input,id,fileName,command)) {throw new WrongFormatException("Not allowed format of request.");}

        this.fileName = makeFileName(fileName);
        this.command = makeCommand(command);
        this.id = makeId(id);
        if (this.fileName != null) {
            this.extension = makeExtension(fileName.substring(fileName.lastIndexOf('.')));
        }

    }

    private boolean isValidInput(String input, String id, String fileName, String command) throws WrongFormatException{

        try {
            if(input == null) {throw new WrongFormatException("Empty input command. Try again.");}
            if(!PatternMatcher.isValidFileName(fileName)) {return false;}
            String result = "";
            if(command!=null) {result = input.replaceFirst(command, "");}
            if(fileName!=null) {result = result.replaceFirst(fileName, "").replaceAll("\"","");}
            if(id!=null) {result = result.replace(id, "");}
            return result.isBlank();
        } catch (PatternSyntaxException e){
            return false;
        }

    }

    private CommandType makeCommand(String command) throws WrongFormatException {
        return CommandType.getValue(command);
    }

    private String makeFileName(String input) throws WrongFormatException {

        if(PatternMatcher.isValidFileName(input)){
            return input;
        } else{
            throw new WrongFormatException("Wrong file name (not allowed symbol).");
        }

    }

    private Extension makeExtension(String ext) throws WrongFormatException {
        return Extension.getValue(ext);
    }

    private Long makeId(String id) throws WrongFormatException {

        if (id == null) return null;
        if (id.length() != 19) {
            throw new WrongFormatException("Id is not 19-digit number.");
        }
        return Long.parseLong(id);

    }

    public CommandType getCommand() {
        return command;
    }

    public Long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public Extension getExtension() {
        return extension;
    }

    @Override
    public String toString() {
        return "Command{" +
                "command=" + command +
                ", id=" + id +
                ", fileName='" + fileName + '\'' +
                ", extension=" + extension +
                '}';
    }

}
