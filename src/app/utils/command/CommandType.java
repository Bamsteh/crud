package src.app.utils.command;

import src.app.utils.exceptions.WrongFormatException;

public enum CommandType {
    CREATE("create"),
    READ("read"),
    UPDATE("update"),
    DELETE("delete"),
    SWITCH("switch"),
    EXIT("exit"),
    HELP("help");

    private String value;

    CommandType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }

    public static CommandType getValue(String value) {
        for(CommandType v : values())
            if(v.getValue().equalsIgnoreCase(value)) return v;
        throw new WrongFormatException("Not allowed command.");
    }

}
