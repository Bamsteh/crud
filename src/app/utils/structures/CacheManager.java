package src.app.utils.structures;

import src.app.models.Person;
import src.app.utils.Utils;
import src.app.utils.exceptions.NoSuchIdException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CacheManager {

    private HashMap<Long, Person> hashPeople = new HashMap<>();


    public CacheManager() {
    }

    public CacheManager(List<Person> people) {
        for(Person person : people){
            this.add(person);
        }
    }

    public CacheManager(HashMap<Long, Person> hashPeople) {
        this.hashPeople = hashPeople;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(Utils.STRING_DELIMITER + "\n");
        for (Person p: this.hashPeople.values()) {
            builder.append(p.toLine()).append("\n");
        }
        builder.append(Utils.STRING_DELIMITER);
        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CacheManager that = (CacheManager) o;
        return Objects.equals(hashPeople, that.hashPeople);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hashPeople);
    }

    public boolean add(Person person){
        if(hashPeople.containsKey(person.getId())){
            return false;
        }
        else {
            hashPeople.put(person.getId(), person);
            return true;
        }
    }

    public boolean contains(long id){
        return this.hashPeople.containsKey(id);
    }

    public boolean isEmpty(){
        return this.hashPeople.isEmpty();
    }

    public Person get(long id){
        if (this.contains(id))
            return this.hashPeople.get(id);
        else throw new NoSuchIdException("There is no such id");
    }

    public CacheManager clear(){
        CacheManager result = new CacheManager(this.hashPeople);
        this.hashPeople.clear();
        return result;
    }

    public Person remove(long id){
        Person buff = this.hashPeople.get(id);
        this.hashPeople.remove(id);
        return buff;
    }

    public Person update(Person updatedPerson){
        if(this.hashPeople.containsKey(updatedPerson.getId())){
            this.hashPeople.remove(updatedPerson.getId());
            this.hashPeople.put(updatedPerson.getId(),updatedPerson);
            return updatedPerson;
        }
        throw new NoSuchIdException("There is no such Id", new IllegalArgumentException());
    }

    public ArrayList<Person> getPeople() {
        return new ArrayList<>(this.hashPeople.values());
    }

}
