package src.app.utils;

public class ExitKey {

    private static boolean exitKey = false;

    public static boolean getExitKey() {
        return exitKey;
    }

    public static void initiateExit(){
        exitKey = true;
    }

}
