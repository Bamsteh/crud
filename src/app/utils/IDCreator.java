package src.app.utils;

import src.app.models.Person;
import src.app.utils.structures.CacheManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IDCreator {

    private List<Person> people = new ArrayList<>();

    public static long newId(CacheManager cacheManager){
        long id;
        Random random = new Random();
        long upper_range = 8223372036854775807L;
        long lower_range = 1000000000000000000L;
        do{
            id =lower_range + (long)(random.nextDouble()*(upper_range));
        } while (cacheManager.contains(id));

        return id;
    }




}
