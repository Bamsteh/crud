package src.app.utils.exceptions;

public class ExitException extends RuntimeException{
    public ExitException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public ExitException(String errorMessage) {
        super(errorMessage);
    }
}
