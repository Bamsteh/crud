package src.app.utils.exceptions;

public class WrongFormatException extends IllegalArgumentException{
    public WrongFormatException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public WrongFormatException(String errorMessage) {
        super(errorMessage);
    }
}
