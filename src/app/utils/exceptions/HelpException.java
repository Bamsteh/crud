package src.app.utils.exceptions;

public class HelpException extends RuntimeException{

    public HelpException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public HelpException(String errorMessage) {
        super(errorMessage);
    }
}
