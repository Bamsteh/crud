package src.app.utils.exceptions;

public class FIleNotExistsException extends RuntimeException{

    public FIleNotExistsException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public FIleNotExistsException(String errorMessage) {
        super(errorMessage);
    }
}
