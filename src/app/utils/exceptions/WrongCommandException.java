package src.app.utils.exceptions;

public class WrongCommandException extends IllegalArgumentException{
    public WrongCommandException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public WrongCommandException(String errorMessage) {
        super(errorMessage);
    }
}
