package src.app.utils.exceptions;

public class SameIdException extends IllegalArgumentException{
    public SameIdException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public SameIdException(String errorMessage) {
        super(errorMessage);
    }
}
