package src.app.utils.exceptions;

public class NullArgumentException extends IllegalArgumentException{
    public NullArgumentException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public NullArgumentException(String errorMessage) {
        super(errorMessage);
    }

    public NullArgumentException() {
        super("Null argument");
    }
}
