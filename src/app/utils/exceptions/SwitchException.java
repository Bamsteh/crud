package src.app.utils.exceptions;

public class SwitchException extends RuntimeException{
    public SwitchException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

    public SwitchException(String errorMessage) {
        super(errorMessage);
    }
}
