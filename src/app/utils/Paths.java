package src.app.utils;

import src.app.utils.command.Extension;

import java.io.File;

public class Paths {
    private static final String personBasePath = "src" + File.separator + "storage";
    private static final String binBasePath =  personBasePath.concat(File.separator + "bin" + File.separator);
    private static final String csvBasePath =  personBasePath.concat(File.separator + "csv" + File.separator);
    private static final String jsonBasePath =  personBasePath.concat(File.separator + "json" + File.separator);
    private static final String xmlBasePath =  personBasePath.concat(File.separator + "xml" + File.separator);
    private static final String ymlBasePath =  personBasePath.concat(File.separator + "yml" + File.separator);
    private static final String helpPath = "src" + File.separator + "storage" + File.separator + "Help.txt";

    public static String getBinBasePath() {
        return binBasePath;
    }

    public static String getCsvBasePath() {
        return csvBasePath;
    }

    public static String getJsonBasePath() {
        return jsonBasePath;
    }

    public static String getXmlBasePath() {
        return xmlBasePath;
    }

    public static String getYmlBasePath() {
        return ymlBasePath;
    }

    public static String getHelpPath() {
        return helpPath;
    }

    public static String definePath(String fileName, Extension extension){
        switch (extension){
            case CSV:
                return Paths.getCsvBasePath() + fileName;
            case JSON:
                return Paths.getJsonBasePath() + fileName;
            case XML:
                return Paths.getXmlBasePath() + fileName;
            case YML:
                return Paths.getYmlBasePath() + fileName;
            case BIN:
                return Paths.getBinBasePath() + fileName;
            default:
                //never called
                throw new IllegalArgumentException("Wrong extension");
        }
    }
}
