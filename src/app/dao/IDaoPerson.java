package src.app.dao;

import src.app.models.Person;
import src.app.utils.structures.CacheManager;

import java.util.ArrayList;

public interface IDaoPerson {

    Person create(Person createdPerson);
    CacheManager createAll(ArrayList<Person> createdPeople);

    Person read(long id);
    CacheManager readAll();

    Person update(Person updatedPerson);
    CacheManager updateAll(ArrayList<Person> updatedPeople);

    Person delete(long id);
    CacheManager deleteAll();

    String getFileName();

    CacheManager getCache();

}
