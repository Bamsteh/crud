package src.app.dao;

import src.app.formats.BinFormat;
import src.app.config.FM.FMBin;
import src.app.models.Person;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.exceptions.NoSuchIdException;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.structures.CacheManager;

import java.util.ArrayList;

public class DaoPersonBin implements IDaoPerson {

    private final CacheManager cacheManager;

    private final BinFormat format;

    private final String fileName;

    @Override
    public CacheManager getCache() {
        return cacheManager;
    }

    public DaoPersonBin(String fileName) {
        this.format = FMBin.binFormat();
        this.fileName = fileName;
        this.cacheManager = new CacheManager(format.fromFormat(FileUtils.readBinFile(fileName)));

    }

    @Override
    public Person create(Person createdPerson) {
        if(createdPerson == null){
            throw new WrongFormatException("Null argument exception.");
        }
        cacheManager.add(createdPerson);
        FileUtils.writeToBinFile(format.toFormat(cacheManager.getPeople()), fileName);
        return createdPerson;
    }

    @Override
    public CacheManager createAll(ArrayList<Person> people) {
        if(people == null){
            throw new WrongFormatException("Null argument exception.");
        }
        for (Person person: people) {
            cacheManager.add(person);
        }
        FileUtils.writeToBinFile(format.toFormat(cacheManager.getPeople()), fileName);
        return cacheManager;
    }

    @Override
    public Person read(long id) {
        if(this.cacheManager.contains(id)){
            Person tempPerson = cacheManager.get(id);
            IOPrinter.printLine(tempPerson.toString());
            return tempPerson;
        }
        else {
            throw new NoSuchIdException("There is no person with such id in this file.");
        }
    }

    @Override
    public CacheManager readAll() {
        IOPrinter.printLine(this.fileName);
        IOPrinter.printLine(this.cacheManager.toString());
        return this.cacheManager;
    }

    @Override public Person update(Person updatedPerson) {
        if(updatedPerson == null){ throw new WrongFormatException("Null argument exception.");
        } try {
            cacheManager.update(updatedPerson);
            FileUtils.writeToBinFile(format.toFormat(cacheManager.getPeople()), fileName);
            return updatedPerson;
        } catch (NoSuchIdException e){
            throw new NoSuchIdException(e.getMessage());
        }
    }

    @Override
    public CacheManager updateAll(ArrayList<Person> updatedPeople) {
        if(updatedPeople == null ||
                updatedPeople.size() != cacheManager.getPeople().size()){
            throw new WrongFormatException("Sizes of arrays are not same.");
        }
        try {
            for (Person updatedPerson : updatedPeople) {
                cacheManager.update(updatedPerson);
            } } catch (NoSuchIdException e){
            throw new NoSuchIdException(e.getMessage()); }
        FileUtils.writeToBinFile(format.toFormat(cacheManager.getPeople()), fileName);
        return cacheManager;
    }

    @Override
    public Person delete(long id) {
        Person temp;
        if(this.cacheManager.contains(id)){
            temp =  this.cacheManager.remove(id);
            FileUtils.writeToBinFile(format.toFormat(cacheManager.getPeople()), fileName);
            return temp;
        }
        else{
            throw new NoSuchIdException("There is no person with such id in this file.");
        }
    }

    @Override
    public CacheManager deleteAll(){
        CacheManager tempCacheManager = cacheManager.clear();
        FileUtils.writeToBinFile(format.toFormat(cacheManager.getPeople()), fileName);
        return tempCacheManager;
    }

    @Override
    public String getFileName(){ return fileName; }

}
