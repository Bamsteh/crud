package src.app.dao;

import src.app.formats.BaseFormat;
import src.app.config.FM.FMJson;
import src.app.models.Person;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.exceptions.NoSuchIdException;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.structures.CacheManager;
import src.app.utils.command.Extension;

import java.util.ArrayList;

public class DaoPersonJson implements IDaoPerson {

    private CacheManager cacheManager;

    private final BaseFormat format;

    private final String fileName;

    @Override
    public CacheManager getCache() {
        return cacheManager;
    }

    public DaoPersonJson(String fileName) {
        this.format = FMJson.jsonFormat();
        this.fileName = fileName;
        this.cacheManager = new CacheManager(format.fromFormat(FileUtils.readFile(fileName, Extension.JSON)));

    }

    @Override
    public Person create(Person createdPerson) {
        if(createdPerson == null){
            throw new WrongFormatException("Null argument exception.");
        }
        cacheManager.add(createdPerson);
        FileUtils.writeToFile(fileName, Extension.JSON, format.toFormat(cacheManager.getPeople()));
        return createdPerson;
    }

    @Override
    public CacheManager createAll(ArrayList<Person> people) {
        if(people == null){
            throw new WrongFormatException("Null argument exception.");
        }
        for (Person person: people) {
            cacheManager.add(person);
        }
        FileUtils.writeToFile(fileName, Extension.JSON, format.toFormat(cacheManager.getPeople()));
        return cacheManager;
    }

    @Override
    public Person read(long id) {
        if(this.cacheManager.contains(id)){
            Person tempPerson = cacheManager.get(id);
            IOPrinter.printLine(tempPerson.toString());
            return tempPerson;
        }
        else {
            throw new NoSuchIdException("There is no person with such id in this file.");
        }
    }

    @Override
    public CacheManager readAll() {
        IOPrinter.printLine(this.fileName);
        IOPrinter.printLine(this.cacheManager.toString());
        return this.cacheManager;
    }

    @Override public Person update(Person updatedPerson) {
        if(updatedPerson == null){ throw new WrongFormatException("Null argument exception.");
        } try {
            cacheManager.update(updatedPerson);
            FileUtils.writeToFile(fileName, Extension.JSON, format.toFormat(cacheManager.getPeople()));
            return updatedPerson;
        } catch (NoSuchIdException e){
            throw new NoSuchIdException(e.getMessage());
        }
    }

    @Override
    public CacheManager updateAll(ArrayList<Person> updatedPeople) {
        if(updatedPeople == null ||
                updatedPeople.size() != cacheManager.getPeople().size()){
            throw new WrongFormatException("Sizes of arrays are not same.");
        }
        try {
            for (Person updatedPerson : updatedPeople) {
                cacheManager.update(updatedPerson);
            } } catch (NoSuchIdException e){
            throw new NoSuchIdException(e.getMessage()); }
        FileUtils.writeToFile(fileName, Extension.JSON, format.toFormat(cacheManager.getPeople()));
        return cacheManager;
    }

    @Override
    public Person delete(long id) {
        Person temp;
        if(this.cacheManager.contains(id)){
            temp =  this.cacheManager.remove(id);
            FileUtils.writeToFile(fileName, Extension.JSON, format.toFormat(cacheManager.getPeople()));
            return temp;
        }
        else{
            throw new NoSuchIdException("There is no person with such id in this file.");
        }
    }

    @Override
    public CacheManager deleteAll(){
        CacheManager tempCacheManager = cacheManager.clear();
        FileUtils.writeToFile(fileName, Extension.JSON, format.toFormat(cacheManager.getPeople()));
        return tempCacheManager;
    }

    @Override
    public String getFileName(){ return fileName; }
}
