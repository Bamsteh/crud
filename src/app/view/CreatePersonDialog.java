package src.app.view;

import src.app.models.Person;
import src.app.utils.ExitKey;
import src.app.utils.IDCreator;
import src.app.utils.Utils;
import src.app.utils.exceptions.ExitException;
import src.app.utils.exceptions.SameIdException;
import src.app.utils.exceptions.SwitchException;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.structures.CacheManager;
import src.app.utils.IO.IOScanner;
import src.app.utils.IO.IOPrinter;

import java.util.ArrayList;

public class CreatePersonDialog {

    public static Person createPerson(CacheManager cacheManager){
        if(cacheManager == null){
            throw new WrongFormatException("CacheManager is null.");
        }
        try {
            Person createdPerson = new Person(IDCreator.newId(cacheManager), "----------", "----------", Utils.noAge, "----------");
            UpdatePersonDialog.updatePerson(createdPerson);
            return createdPerson;
        }
        catch (SwitchException e){
            throw new SwitchException("Switched from creation dialog.");
        }
    }

    public static Person createPerson(Long id, CacheManager cacheManager){
        if(cacheManager == null){
            throw new WrongFormatException("CacheManager is null.");
        }
        try {
            if(cacheManager.contains(id)){
                throw new SameIdException("There already is such id.");
            }
            Person createdPerson = new Person(id, "firstName", "lastName", -7777, "City");
            UpdatePersonDialog.updatePerson(createdPerson);
            return createdPerson;
        }
        catch (SwitchException e){
            throw new SwitchException("Switched from creation dialog.");
        }
    }

    public static ArrayList<Person> createPeople(CacheManager cacheManager){
        if(cacheManager == null){
            throw new WrongFormatException("CacheManager is null.");
        }
        ArrayList<Person> result = new ArrayList<>(0);
        Person createdPerson;
        try {
            do {
                createdPerson = createPerson(cacheManager);
                result.add(createdPerson);
            } while (CreatePersonDialog.createAnotherPerson());
        }
        catch (ExitException e){
            ExitKey.initiateExit();
        }
        catch (SwitchException ignore){ }

        return result;
    }

    public static boolean createAnotherPerson(){
        while (true) {
            IOPrinter.printLine("Would you like to create another person? (y/n)");
            String answer = IOScanner.readLine().toLowerCase();
            switch (answer){
                case "y":
                case "yes":
                    return true;
                case "n":
                case "no":
                    return false;
            }
        }
    }
}
