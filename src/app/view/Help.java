package src.app.view;

import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.Paths;

public class Help {
    public static void helpMessage(){
        String help = FileUtils.readFile(Paths.getHelpPath());
        IOPrinter.printLine(help);
    }
}
