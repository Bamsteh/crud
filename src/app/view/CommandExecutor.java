package src.app.view;

import src.app.config.FDaoPerson;
import src.app.dao.IDaoPerson;
import src.app.config.FM.*;
import src.app.models.Person;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.Paths;
import src.app.utils.exceptions.*;
import src.app.utils.command.Command;
import src.app.utils.command.Extension;
import src.app.utils.command.CommandType;

import java.io.File;
import java.util.ArrayList;

public class CommandExecutor {

    private IDaoPerson dao;

    public void execute(Command command){

        if(command == null){
            return;
        }

        try {
            executeHelpers(command);
        } catch (SwitchException | HelpException e){
            return;
        }

        if(command.getFileName() != null){
            try {
                createNotExistingFile(command.getCommand() == CommandType.CREATE,
                        command.getFileName(), command.getExtension());
            } catch (FIleNotExistsException e){
                IOPrinter.printLine(e.getMessage());
                return;
            }
            dao = FDaoPerson.chooseDao(command.getFileName(), command.getExtension());
        } else{
            if(dao == null){
                IOPrinter.printLine("No opened file. First choose file to work with.");
                return;
            }
        }
        if(command.getId() == null){
            executeCommandAll(command.getCommand());
        } else{
            executeCommandId(command.getCommand(), command.getId());
        }
    }


    private void executeHelpers(Command command){
        switch (command.getCommand()){
            case EXIT:
                if(ExitConfirmation.confirm()){
                    throw new ExitException("Quiting the program...");
                }
                throw new SwitchException("Switched from exit.");
            case SWITCH:
                if(dao != null) {
                    IOPrinter.printLine("Removing file \"" + dao.getFileName() + "\" from memory...");
                    dao = null;
                }
                throw new SwitchException("");
            case HELP:
                Help.helpMessage();
                throw new HelpException("");
        }
    }


    private void createNotExistingFile(boolean isCreateCommand, String fileName, Extension extension){
        File file = new File(Paths.definePath(fileName, extension));
        if(!file.exists()){
            IOPrinter.printLine("File with this name does not exists.");
            if(!isCreateCommand){
                throw new FIleNotExistsException("Create a file with command \"create\" first.");
            } else {
                IOPrinter.printLine("Creating a file \"" + fileName + "\"...");
                FileUtils.createFile(fileName, extension);
                ArrayList<Person> emptyPeople = new ArrayList<>(0){};
                switch (extension){
                    case BIN:
                        FileUtils.writeToBinFile(FMBin.binFormat().toFormat(emptyPeople), fileName);
                        break;
                    case CSV:
                        FileUtils.writeToFile(fileName, Extension.CSV, FMCsv.csvFormat().toFormat(emptyPeople));
                        break;
                    case JSON:
                        FileUtils.writeToFile(fileName, Extension.JSON, FMJson.jsonFormat().toFormat(emptyPeople));
                        break;
                    case XML:
                        FileUtils.writeToFile(fileName, Extension.XML, FMXml.xmlFormat().toFormat(emptyPeople));
                        break;
                    case YML:
                        FileUtils.writeToFile(fileName, Extension.YML, FMYml.ymlFormat().toFormat(emptyPeople));
                        break;
                }
                IOPrinter.printLine("File was created successfully...");
            }
        }
    }

    private void executeCommandAll(CommandType command){
        try {
            switch (command){
                case CREATE:
                    IOPrinter.printLine("Creating new content for \"" + dao.getFileName() + "\" file...");
                    dao.createAll(CreatePersonDialog.createPeople(dao.getCache()));
                    IOPrinter.printLine("New content was created successfully...");
                    break;
                case READ:
                    IOPrinter.printLine("Reading content of \"" + dao.getFileName() + "\" file...");
                    dao.readAll();
                    break;
                case UPDATE:
                    IOPrinter.printLine("Updating content of \"" + dao.getFileName() + "\" file...");
                    dao.updateAll(UpdatePersonDialog.updatePeople(dao.getCache().getPeople()));
                    IOPrinter.printLine("Content was updated successfully...");
                    break;
                case DELETE: {
                    IOPrinter.printLine("Clearing content of \"" + dao.getFileName() + "\" file...");
                    dao.deleteAll();
                    IOPrinter.printLine("File was cleared successfully...");
                    break;
                }
            }
        } catch (SwitchException e){
            IOPrinter.printLine(e.getMessage());
        }
    }

    private void executeCommandId(CommandType command, long id){
        try{
            switch (command){
                case CREATE:
                    IOPrinter.printLine("Creating person with " + id + " id in \"" + dao.getFileName() + "\" file...");
                    dao.create(CreatePersonDialog.createPerson(id, dao.getCache()));
                    IOPrinter.printLine("Person was created successfully...");
                    break;
                case READ:
                    IOPrinter.printLine("Reading person with " + id + " id in \"" + dao.getFileName() + "\" file...");
                    dao.read(id);
                    break;
                case UPDATE:
                    IOPrinter.printLine("Updating person with " + id + " id in \"" + dao.getFileName() + "\" file...");
                    dao.update(UpdatePersonDialog.updatePerson(dao.getCache().get(id)));
                    IOPrinter.printLine("Person was updated successfully...");
                    break;
                case DELETE:
                    IOPrinter.printLine("Deleting person with " + id + " id in \"" + dao.getFileName() + "\" file...");
                    dao.delete(id);
                    IOPrinter.printLine("Person was deleted successfully...");
                    break;
            }
        } catch (NoSuchIdException | SwitchException | SameIdException e){
            IOPrinter.printLine(e.getMessage());
        }
    }
}
