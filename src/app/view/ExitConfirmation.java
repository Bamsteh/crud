package src.app.view;

import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;

public class ExitConfirmation {
    public static boolean confirm(){
        while (true){
            IOPrinter.printLine("Are you sure you want to exit? (y/n)");
            String answer = IOScanner.readLine().toLowerCase();
            switch (answer){
                case "y":
                case "yes":
                    return true;
                case "n":
                case "no":
                case "switch":
                    return false;
            }
        }
    }
}
