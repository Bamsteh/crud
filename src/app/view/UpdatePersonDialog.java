package src.app.view;

import src.app.models.Person;
import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;
import src.app.utils.PatternMatcher;
import src.app.utils.Utils;
import src.app.utils.exceptions.ExitException;
import src.app.utils.exceptions.SwitchException;
import src.app.utils.exceptions.WrongFormatException;

import java.util.ArrayList;

public class UpdatePersonDialog {

    public static Person updatePerson(Person personToUpdate){

        IOPrinter.printLine(Utils.STRING_DELIMITER);

        if(personToUpdate == null){
            throw new WrongFormatException("Null person exception");
        }
        String firstName, lastName, city;
        int age;
        String buffer;
        Person tempPerson = new Person(personToUpdate);
        boolean notFirstIteration = false;
        IOPrinter.printLine(tempPerson.toLine());

        do {
            if(notFirstIteration){
                IOPrinter.printLine("Name is not valid");
            }
            IOPrinter.print("First name:\t");
            firstName = IOScanner.readLine();
            firstName = firstName.trim();
            if(firstName.equals("switch")){
                IOPrinter.printLine(Utils.STRING_DELIMITER);
                throw new SwitchException("Switched from update dialog.");
            }
            if(firstName.equals("exit")){
                if(ExitConfirmation.confirm()){
                    throw new ExitException("Quiting the program.");
                } else {
                    firstName = Utils.notValidName;
                    continue;
                }
            }
            notFirstIteration = true;
        } while (!PatternMatcher.isValidName(firstName));

        tempPerson.setFirstName(firstName);
        IOPrinter.printLine(tempPerson.toLine());

        notFirstIteration = false;
        do {
            if(notFirstIteration){
                IOPrinter.printLine("Name is not valid");
            }
            IOPrinter.print("Last name:\t");
            lastName = IOScanner.readLine();
            lastName = lastName.trim();
            if(lastName.equals("switch")){
                IOPrinter.printLine(Utils.STRING_DELIMITER);
                throw new SwitchException("Switched from update dialog.");
            }
            if(lastName.equals("exit")){
                if(ExitConfirmation.confirm()){
                    throw new ExitException("Quiting the program.");
                } else {
                    lastName = Utils.notValidName;
                    continue;
                }
            }
            notFirstIteration = true;
        } while (!PatternMatcher.isValidName(lastName));

        tempPerson.setLastName(lastName);
        IOPrinter.printLine(tempPerson.toLine());

        while (true){
            IOPrinter.print("Age:\t");
            buffer = IOScanner.readLine();
            buffer = buffer.trim();
            if(buffer.equals("switch")){
                IOPrinter.printLine(Utils.STRING_DELIMITER);
                throw new SwitchException("Switched from update dialog.");
            }
            if(buffer.equals("exit")){
                if(ExitConfirmation.confirm()){
                    throw new ExitException("Quiting the program.");
                } else {
                    continue;
                }
            }
            try {
                age = Integer.parseInt(buffer);
                if(age < 0){
                    IOPrinter.printLine("Age can not be a negative number.");
                    continue;
                }
                tempPerson.setAge(age);
                break;
            }
            catch (Exception e){
                IOPrinter.printLine("Age must be an integer number.");
            }
        }

        IOPrinter.printLine(tempPerson.toLine());

        notFirstIteration = false;
        do {
            if(notFirstIteration){
                IOPrinter.printLine("Name is not valid.");
            }
            IOPrinter.print("City:\t");
            city = IOScanner.readLine();
            city = city.trim();
            if(city.equals("switch")){
                IOPrinter.printLine(Utils.STRING_DELIMITER);
                throw new SwitchException("Switched from update dialog.");
            }
            if(city.equals("exit")){
                if(ExitConfirmation.confirm()){
                    throw new ExitException("Quiting the program.");
                } else {
                    city = Utils.notValidName;
                    continue;
                }
            }
            notFirstIteration = true;
        } while (!PatternMatcher.isValidName(city));

        tempPerson.setCity(city);
        IOPrinter.printLine(tempPerson.toLine());

        IOPrinter.printLine(Utils.STRING_DELIMITER);

        personToUpdate.setFirstName(firstName);
        personToUpdate.setLastName(lastName);
        personToUpdate.setAge(age);
        personToUpdate.setCity(city);
        return personToUpdate;
    }

    public static ArrayList<Person> updatePeople(ArrayList<Person> peopleToUpdate){
        if(peopleToUpdate == null){
            throw new WrongFormatException("Null people exception");
        }
        for (Person person : peopleToUpdate) {
            UpdatePersonDialog.updatePerson(person);
        }
        return peopleToUpdate;
    }
}
