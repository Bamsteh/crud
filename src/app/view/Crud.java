package src.app.view;

import src.app.utils.ExitKey;
import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;
import src.app.utils.exceptions.ExitException;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.command.Command;

public class Crud {

    private static void helloIcon(){
        String CRUD = "" +
                "*******************************************************\n" +
                "*  *********   **********   **        **  *********   *\n" +
                "* **       **  **       **  **        **  **      **  *\n" +
                "* **           **      ***  **        **  **       ** *\n" +
                "* **           **********   **        **  **       ** *\n" +
                "* **           **      ***  **        **  **       ** *\n" +
                "* **       **  **       **  ***      ***  **      **  *\n" +
                "*  *********   **       **   **********   *********   *\n" +
                "*******************************************************\n";
        IOPrinter.printLine(CRUD);
    }

    public void run(){

        helloIcon();
        IOPrinter.printLine("Type \"help\" to call help.");

        CommandExecutor executor = new CommandExecutor();
        do {
            try {
                executor.execute(new Command(IOScanner.readLine()));
            } catch (WrongFormatException e){
                IOPrinter.printLine(e.getMessage());
            } catch (ExitException e){
                ExitKey.initiateExit();
            }
        } while (!ExitKey.getExitKey());
        IOPrinter.printLine("Quiting the program...");
    }
}
