package src.app.formats;

import src.app.models.Person;

import java.util.List;

public interface BaseFormat{

    List<Person> fromFormat(String file);

    String toFormat(List<Person> person);

}