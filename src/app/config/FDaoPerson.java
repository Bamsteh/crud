package src.app.config;

import src.app.dao.IDaoPerson;
import src.app.config.FM.*;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.command.Extension;

public class FDaoPerson {

    public static IDaoPerson chooseDao(String fileName, Extension extension){

        switch (extension){
            case BIN:
                return FMBin.daoPersonBin(fileName);
            case CSV:
                return FMCsv.daoPersonCsv(fileName);
            case JSON:
                return FMJson.daoPersonJson(fileName);
            case XML:
                return FMXml.daoPersonXml(fileName);
            case YML:
                return FMYml.daoPersonYml(fileName);
        }
        throw new WrongFormatException("Wrong extension.");
    }
}
