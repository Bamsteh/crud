package src.app.config.FM;

import src.app.dao.DaoPersonBin;
import src.app.dao.IDaoPerson;
import src.app.formats.BinFormat;

public class FMBin {

    public static BinFormat binFormat(){
        return new BinFormat();
    }

    public static IDaoPerson daoPersonBin(String fileName){
        return new DaoPersonBin(fileName);
    }
}
