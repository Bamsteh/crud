package src.app.config.FM;

import src.app.dao.DaoPersonYml;
import src.app.dao.IDaoPerson;
import src.app.formats.BaseFormat;
import src.app.formats.YmlFormat;

public class FMYml {

    public static BaseFormat ymlFormat(){
        return new YmlFormat();
    }

    public static IDaoPerson daoPersonYml(String fileName){
        return new DaoPersonYml(fileName);
    }

}
