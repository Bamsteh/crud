package src.app.config.FM;

import src.app.dao.DaoPersonJson;
import src.app.dao.IDaoPerson;
import src.app.formats.BaseFormat;
import src.app.formats.JsonFormat;

public class FMJson {
    public static BaseFormat jsonFormat(){
        return new JsonFormat();
    }

    public static IDaoPerson daoPersonJson(String fileName){
        return new DaoPersonJson(fileName);
    }
}
