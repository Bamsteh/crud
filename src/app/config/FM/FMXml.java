package src.app.config.FM;

import src.app.dao.DaoPersonXml;
import src.app.dao.IDaoPerson;
import src.app.formats.BaseFormat;
import src.app.formats.XmlFormat;

public class FMXml {

        public static BaseFormat xmlFormat(){
            return new XmlFormat();
        }

        public static IDaoPerson daoPersonXml(String fileName){
            return new DaoPersonXml(fileName);
        }

}
