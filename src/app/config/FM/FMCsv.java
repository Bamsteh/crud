package src.app.config.FM;

import src.app.dao.DaoPersonCsv;
import src.app.dao.IDaoPerson;
import src.app.formats.BaseFormat;
import src.app.formats.CsvFormat;

public class FMCsv {
    public static BaseFormat csvFormat(){
        return new CsvFormat();
    }

    public static IDaoPerson daoPersonCsv(String fileName){
        return new DaoPersonCsv(fileName);
    }
}
