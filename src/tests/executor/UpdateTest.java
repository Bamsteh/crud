package src.tests.executor;

import org.junit.*;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;
import src.app.utils.Utils;
import src.app.utils.command.Command;
import src.app.utils.command.Extension;
import src.app.view.CommandExecutor;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

public class UpdateTest {

    private static final CommandExecutor executor = new CommandExecutor();

    private static ByteArrayInputStream inputStream;

    @Before
    public void setUp(){
        FileUtils.createFile("test.csv", Extension.CSV);
        FileUtils.writeToFile("test.csv", Extension.CSV,
                "id,firstName,lastName,age,city\n" +
                        "1111111111111111111, firstName1, lastName1, 11, city1\n" +
                        "2222222222222222222, firstName2, lastName2, 22, city2\n");
        IOPrinter.printLine(Utils.STRING_DELIMITER);
    }

    @Test
    public void updateAll(){
        inputStream = new ByteArrayInputStream((
                "newFirstName1" + System.lineSeparator() +
                "newLastName1" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity1" + System.lineSeparator() +
                "newFirstName2" + System.lineSeparator() +
                "newLastName2" + System.lineSeparator() +
                "222" + System.lineSeparator() +
                "newCity2" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("update test.csv"));
    }

    @Test
    public void updateById(){
        inputStream = new ByteArrayInputStream((
            "newFirstName1" + System.lineSeparator() +
            "newLastName1" + System.lineSeparator() +
            "111" + System.lineSeparator() +
            "newCity1" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("update test.csv 1111111111111111111"));
    }

    @Test
    public void updateOpenedFile(){
        executor.execute(new Command("read test.csv"));
        inputStream = new ByteArrayInputStream((
            "newFirstName1" + System.lineSeparator() +
            "newLastName1" + System.lineSeparator() +
            "111" + System.lineSeparator() +
            "newCity1" + System.lineSeparator() +
            "newFirstName2" + System.lineSeparator() +
            "newLastName2" + System.lineSeparator() +
            "222" + System.lineSeparator() +
            "newCity2" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("update"));
    }

    @Test
    public void updateOpenedFileById(){
        executor.execute(new Command("read test.csv"));
        inputStream = new ByteArrayInputStream((
            "newFirstName2" + System.lineSeparator() +
            "newLastName2" + System.lineSeparator() +
            "222" + System.lineSeparator() +
            "newCity2" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("update 2222222222222222222"));
    }

    @Test
    public void updateNotExistingId(){
        executor.execute(new Command("update test.csv 3333333333333333333"));
    }

    @Test
    public void updateNotExistingFile(){
        executor.execute(new Command("update text.json"));
    }

    @After
    public void tearDown(){
        IOScanner.setDefaultInputStream();
        FileUtils.deleteFile("test.csv", Extension.CSV);
    }
}
