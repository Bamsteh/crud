package src.tests.executor;

import org.junit.Test;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.command.Command;
import src.app.view.CommandExecutor;

public class WrongCommandTest {

    private final static CommandExecutor executor = new CommandExecutor();

    @Test(expected = WrongFormatException.class)
    public void wrongCommand(){
        executor.execute(new Command("wrongCommand"));
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExtension(){
        executor.execute(new Command("read some.some"));
    }

    @Test(expected = WrongFormatException.class)
    public void wrongId(){
        executor.execute(new Command("read 13"));
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExtensionWrongId(){
        executor.execute(new Command("read some.some 13"));
    }

    @Test(expected = WrongFormatException.class)
    public void wrongCommandWrongExtensionWrongId(){
        executor.execute(new Command("wrongCommand some.some 13"));
    }

    @Test
    public void nullCommand(){
        executor.execute(null);
    }

}
