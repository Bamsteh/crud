package src.tests.executor;

import org.junit.AfterClass;
import org.junit.Test;
import src.app.utils.IO.IOScanner;
import src.app.utils.exceptions.ExitException;
import src.app.utils.command.Command;
import src.app.view.CommandExecutor;

import java.io.ByteArrayInputStream;

public class ExitTest {

    private final static CommandExecutor executor = new CommandExecutor();

    private ByteArrayInputStream inputStream;

    @Test
    public void exitN(){
        inputStream = new ByteArrayInputStream("n".getBytes());
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("exit"));
    }

    @Test
    public void exitNo(){
        inputStream = new ByteArrayInputStream("no".getBytes());
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("exit"));
    }

    @Test(expected = ExitException.class)
    public void exitY(){
        inputStream = new ByteArrayInputStream("y".getBytes());
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("exit"));
    }

    @Test(expected = ExitException.class)
    public void exitYes(){
        inputStream = new ByteArrayInputStream("yes".getBytes());
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("exit"));
    }

    @AfterClass
    public static void tearDown(){
        IOScanner.setDefaultInputStream();
    }
}
