package src.tests.executor;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;
import src.app.utils.Utils;
import src.app.utils.command.Command;
import src.app.utils.command.Extension;
import src.app.view.CommandExecutor;

public class ReadTest {

    private static final CommandExecutor executor = new CommandExecutor();

    @BeforeClass
    public static void setUp(){
        FileUtils.createFile("test.csv", Extension.CSV);
        FileUtils.writeToFile("test.csv", Extension.CSV,
                "id,firstName,lastName,age,city\n" +
                        "1111111111111111111, firstName1, lastName1, 11, city1\n" +
                        "2222222222222222222, firstName2, lastName2, 22, city2\n");
    }

    @Before
    public void switchFile(){
        IOPrinter.printLine(Utils.STRING_DELIMITER);
        executor.execute(new Command("switch"));
    }

    @Test
    public void read(){
        executor.execute(new Command("read test.csv"));
    }

    @Test
    public void readById(){
        executor.execute(new Command("read test.csv 1111111111111111111"));
    }

    @Test
    public void readOpenedFile(){
        executor.execute(new Command("read test.csv"));
        executor.execute(new Command("read"));
    }

    @Test
    public void readNotExistingId(){
        executor.execute(new Command("read test.csv 3333333333333333333"));
    }

    @Test
    public void readNotExistingFile(){
        executor.execute(new Command("read text.yml"));
    }

    @Test
    public void readNotOpenedFile(){
        executor.execute(new Command("read"));
    }

    @AfterClass
    public static void tearDown(){
        IOScanner.setDefaultInputStream();
        FileUtils.deleteFile("test.csv", Extension.CSV);
    }
}
