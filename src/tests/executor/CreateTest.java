package src.tests.executor;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;
import src.app.utils.Utils;
import src.app.utils.command.Command;
import src.app.utils.command.Extension;
import src.app.view.CommandExecutor;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

public class CreateTest {

    private static final CommandExecutor executor = new CommandExecutor();

    private static ByteArrayInputStream inputStream;

    @Before
    public void printDelimiter(){
        IOPrinter.printLine(Utils.STRING_DELIMITER);
    }

    @Test
    public void createOne(){
        inputStream = new ByteArrayInputStream((
            "newFirstName" + System.lineSeparator() +
            "newLastName" + System.lineSeparator() +
            "12" + System.lineSeparator() +
            "newCity" + System.lineSeparator() +
            "no" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.yml"));
        FileUtils.deleteFile("test.yml", Extension.YML);
    }

    @Test
    public void createTwo(){
        inputStream = new ByteArrayInputStream((
            "newFirstName1" + System.lineSeparator() +
            "newLastName1" + System.lineSeparator() +
            "11" + System.lineSeparator() +
            "newCity1" + System.lineSeparator() +
            "yes" + System.lineSeparator() +
            "newFirstName2" + System.lineSeparator() +
            "newLastName2" + System.lineSeparator() +
            "22" + System.lineSeparator() +
            "newCity2" + System.lineSeparator() +
            "n" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.xml"));
        FileUtils.deleteFile("test.xml", Extension.XML);
    }

    @Test
    public void createMany(){
        inputStream = new ByteArrayInputStream((
            "newFirstName1" + System.lineSeparator() +
            "newLastName1" + System.lineSeparator() +
            "11" + System.lineSeparator() +
            "newCity1" + System.lineSeparator() +
            "yes" + System.lineSeparator() +
            "newFirstName2" + System.lineSeparator() +
            "newLastName2" + System.lineSeparator() +
            "22" + System.lineSeparator() +
            "newCity2" + System.lineSeparator() +
            "y" + System.lineSeparator() +
            "newFirstName3" + System.lineSeparator() +
            "newLastName3" + System.lineSeparator() +
            "33" + System.lineSeparator() +
            "newCity3" + System.lineSeparator() +
            "no" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.csv"));
        FileUtils.deleteFile("test.csv", Extension.CSV);
    }

    @Test
    public void createId(){
        inputStream = new ByteArrayInputStream((
            "newFirstName" + System.lineSeparator() +
            "newLastName" + System.lineSeparator() +
            "12" + System.lineSeparator() +
            "newCity" + System.lineSeparator() +
            "no" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.bin 1111111111111111111"));
        FileUtils.deleteFile("test.bin", Extension.BIN);
    }

    @Test
    public void createNoFileName(){
        inputStream = new ByteArrayInputStream((
            "switch" + System.lineSeparator() +
            "newFirstName" + System.lineSeparator() +
            "newLastName" + System.lineSeparator() +
            "12" + System.lineSeparator() +
            "newCity" + System.lineSeparator() +
            "no" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.yml"));
        executor.execute(new Command("create"));
        FileUtils.deleteFile("test.yml", Extension.YML);
    }

    @Test
    public void switchCreate(){
        inputStream = new ByteArrayInputStream((
            "newFirstName" + System.lineSeparator() +
            "newLastName" + System.lineSeparator() +
            "switch" + System.lineSeparator() +
            "newCity" + System.lineSeparator() +
            "no" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.yml"));
        FileUtils.deleteFile("test.yml", Extension.YML);
    }

    @Test
    public void createSameId(){
        inputStream = new ByteArrayInputStream((
            "firstName1" + System.lineSeparator() +
            "lastName1" + System.lineSeparator() +
            "11" + System.lineSeparator() +
            "city1" + System.lineSeparator() +
            "no" + System.lineSeparator() +
            "newFirstName" + System.lineSeparator() +
            "newLastName" + System.lineSeparator() +
            "111" + System.lineSeparator() +
            "newCity" + System.lineSeparator() +
            "no" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.bin 1111111111111111111"));
        executor.execute(new Command("create test.bin 1111111111111111111"));
        FileUtils.deleteFile("test.bin", Extension.BIN);
    }

    @AfterClass
    public static void tearDown(){
        IOScanner.setDefaultInputStream();
    }
}
