package src.tests.executor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;
import src.app.utils.Utils;
import src.app.utils.command.Command;
import src.app.utils.command.Extension;
import src.app.view.CommandExecutor;

public class DeleteTest {

    private static final CommandExecutor executor = new CommandExecutor();

    @Before
    public void setUp(){
        FileUtils.createFile("test.csv", Extension.CSV);
        FileUtils.writeToFile("test.csv", Extension.CSV,
                "id,firstName,lastName,age,city\n" +
                        "1111111111111111111, firstName1, lastName1, 11, city1\n" +
                        "2222222222222222222, firstName2, lastName2, 22, city2\n");
        IOPrinter.printLine(Utils.STRING_DELIMITER);
    }

    @Test
    public void deleteAll(){
        executor.execute(new Command("delete test.csv"));
    }

    @Test
    public void deleteById(){
        executor.execute(new Command("delete test.csv 1111111111111111111"));
    }

    @Test
    public void deleteNotExistingId(){
        executor.execute(new Command("delete test.csv 3333333333333333333"));
    }

    @Test
    public void deleteOpenedFile(){
        executor.execute(new Command("read test.csv"));
        executor.execute(new Command("delete"));
    }

    @Test
    public void deleteOpenedFileById(){
        executor.execute(new Command("read test.csv"));
        executor.execute(new Command("delete 1111111111111111111"));
    }

    @After
    public void tearDown(){
        IOScanner.setDefaultInputStream();
        FileUtils.deleteFile("test.csv", Extension.CSV);
    }
}
