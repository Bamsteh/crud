package src.tests.executor;

import org.junit.AfterClass;
import org.junit.Test;
import src.app.utils.FileUtils;
import src.app.utils.IO.IOScanner;
import src.app.utils.command.Command;
import src.app.utils.command.Extension;
import src.app.view.CommandExecutor;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

public class SwitchTest {

    private static final CommandExecutor executor =  new CommandExecutor();

    private ByteArrayInputStream inputStream;


    @Test
    public void switchNoFile(){
        executor.execute(new Command("switch"));
    }

    @Test
    public void switchFileCreation(){
        inputStream = new ByteArrayInputStream("switch".getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.yml"));
        FileUtils.deleteFile("test.yml", Extension.YML);
    }

    @Test
    public void switchFile(){
        inputStream = new ByteArrayInputStream((
                "firstName" + System.lineSeparator() +
                "lastName" + System.lineSeparator() +
                "12" + System.lineSeparator() +
                "city" + System.lineSeparator() +
                "no" + System.lineSeparator() +
                "switch"
            ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        executor.execute(new Command("create test.json"));
        FileUtils.deleteFile("test.json", Extension.JSON);
    }

    @AfterClass
    public static void tearDown(){
        IOScanner.setDefaultInputStream();
    }

}
