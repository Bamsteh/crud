package src.tests.executor;

import org.junit.Test;
import src.app.utils.command.Command;
import src.app.view.CommandExecutor;

public class HelpTest {

    private static final CommandExecutor executor = new CommandExecutor();

    @Test
    public void help(){
        executor.execute(new Command("help"));
    }
}
