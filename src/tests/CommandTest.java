package src.tests;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.command.Command;
import src.app.utils.command.CommandType;
import src.app.utils.command.Extension;

public class CommandTest {

    private static Command c1, c2, c3, c4, c5, c6;

    @BeforeClass
    public static void setUp(){
        c1 = new Command("create file.json 1111111111111111111");
        c2 = new Command("delete file.yml");
        c3 = new Command("update 1111111111111111111");
        c4 = new Command("exit");
        c5 = new Command("delete \"file top secret.json\"");
        c6 = new Command("create create.csv");
    }

    @Test
    public void getCommandTest1() {
        Assert.assertEquals(c1.getCommand(), CommandType.CREATE);
    }

    @Test
    public void getCommandTest2(){
        Assert.assertEquals(c2.getCommand(), CommandType.DELETE);
    }

    @Test
    public void getCommandTest3(){
        Assert.assertEquals(c3.getCommand(), CommandType.UPDATE);
    }

    @Test
    public void getCommandTest4(){
        Assert.assertEquals(c4.getCommand(), CommandType.EXIT);
    }

    @Test
    public void getCommandTest5(){
        Assert.assertEquals(c5.getCommand(),CommandType.DELETE);
    }

    @Test
    public void getCommandTest6(){
        Assert.assertEquals(c6.getCommand(),CommandType.CREATE);
    }

    @Test
    public void getIdTest1() {
        Assert.assertEquals(1111111111111111111L,(long) c1.getId());
    }

    @Test
    public void getIdTest2() {
        Assert.assertNull(c2.getId());
    }

    @Test
    public void getIdTest3() {
        Assert.assertEquals(1111111111111111111L,(long) c3.getId());
    }

    @Test
    public void getIdTest4() {
        Assert.assertNull(c4.getId());
    }

    @Test
    public void getIdTest5() {
        Assert.assertNull(c5.getId());
    }

    @Test
    public void getIdTest6() {
        Assert.assertNull(c6.getId());
    }

    @Test
    public void getFileNameTest1() {
        Assert.assertEquals("file.json",c1.getFileName());
    }

    @Test
    public void getFileNameTest2() {
        Assert.assertEquals("file.yml",c2.getFileName());
    }

    @Test
    public void getFileNameTest3() {
        Assert.assertNull(c3.getFileName());
    }

    @Test
    public void getFileNameTest4() {
        Assert.assertNull(c4.getFileName());
    }

    @Test
    public void getFileNameTest5() {
        Assert.assertEquals("file top secret.json",c5.getFileName());
    }

    @Test
    public void getFileNameTest6() {
        Assert.assertEquals("create.csv",c6.getFileName());
    }

    @Test
    public void getExtensionTest1() {
        Assert.assertEquals(Extension.JSON,c1.getExtension());
    }

    @Test
    public void getExtensionTest2() {
        Assert.assertEquals(Extension.YML,c2.getExtension());
    }

    @Test
    public void getExtensionTest3() {
        Assert.assertNull(c3.getExtension());
    }

    @Test
    public void getExtensionTest4() {
        Assert.assertNull(c4.getExtension());
    }

    @Test
    public void getExtensionTest5() {
        Assert.assertEquals(Extension.JSON,c5.getExtension());
    }

    @Test
    public void getExtensionTest6() {
        Assert.assertEquals(Extension.CSV,c6.getExtension());
    }

    @Test
    public void getFileNameSEcomdCase(){
        Assert.assertEquals("file top secret.json",c5.getFileName());
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExtensionExceptionTest(){
        Command c5 = new Command("create file.exe 1111111111111111111");
    }

    @Test(expected = WrongFormatException.class)
    public void wrongCommandExceptionTest(){
        Command c6 = new Command("pull file.json 1111111111111111111");
    }

    @Test(expected = WrongFormatException.class)
    public void wrongIdFormatExceptionTest(){
        Command c7 = new Command("create file.json 13372281488");
    }

    @Test(expected = WrongFormatException.class)
    public void strangeFormatExceptionTest(){
        Command c8 = new Command("qwerqwr");
    }

    @Test(expected = WrongFormatException.class)
    public void somTextExceptionTest(){
        Command c9 = new Command("some text to test it 1123135.5");
    }

    @Test(expected = WrongFormatException.class)
    public void emptyStringExceptionTest(){
        Command c10 = new Command("");
    }

    @Test(expected = WrongFormatException.class)
    public void nullExceptionTest(){
        Command c11 = new Command(null);
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExceptionTest(){
        Command c12 = new Command("create something");
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExceptionTest1(){
        Command c13 = new Command("create file.json something");
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExceptionTest2(){
        Command c14 = new Command("create file.json 1111111111111111111 something");
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExceptionTest3(){
        Command c15 = new Command("create file.json something 1111111111111111111");
    }

    @Test(expected = WrongFormatException.class)
    public void wrongExceptionTest4(){
        Command c14 = new Command("create file.json 1111111111111111111 something");
    }



}