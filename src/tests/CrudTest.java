package src.tests;

import org.junit.Test;
import src.app.utils.IO.IOScanner;
import src.app.view.Crud;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

public class CrudTest {

    @Test
    public void crud(){
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "help" + System.lineSeparator() +
                "wrongCommand" + System.lineSeparator() +
                "switch" + System.lineSeparator() +
                "exit" + System.lineSeparator() +
                "y" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        Crud crud = new Crud();
        crud.run();
    }
}
