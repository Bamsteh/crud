package src.tests.dialogs;

import org.junit.Assert;
import org.junit.Test;
import src.app.models.Person;
import src.app.utils.IO.IOScanner;
import src.app.utils.exceptions.SwitchException;
import src.app.utils.exceptions.WrongFormatException;
import src.app.view.UpdatePersonDialog;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class UpdatePersonDialogTest {

    @Test
    public void updatePerson(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void notIntegerAge(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "notInteger" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void negativeAge(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "-111" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void notValidFirstName(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                ",{}<>" + System.lineSeparator() +
                "newFirstName" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void notValidLastName(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                ",{}<>" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void notValidCity(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                        "newLastName" + System.lineSeparator() +
                        "notInteger" + System.lineSeparator() +
                        "111" + System.lineSeparator() +
                        ",{}<>" + System.lineSeparator() +
                        "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = SwitchException.class)
    public void updatePersonSwitchFirstName(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "switch" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = SwitchException.class)
    public void updatePersonSwitchLastName(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                "switch" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = SwitchException.class)
    public void updatePersonSwitchAge(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "switch" + System.lineSeparator() +
                "newCity" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test(expected = SwitchException.class)
    public void updatePersonSwitchCity(){
        Person exp = new Person(1111111111111111111L,"newFirstName",
                "newLastName", 111, "newCity");
        Person act = new Person(1111111111111111111L,"firstName",
                "lastName", 11, "city");
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName" + System.lineSeparator() +
                "newLastName" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "switch" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePerson(act);
        Assert.assertEquals(exp, act);
    }

    @Test
    public void updatePeople(){
        Person exp0 = new Person(1111111111111111111L,"newFirstName1",
                "newLastName1", 111, "newCity1");
        Person exp1 = new Person(2222222222222222222L,"newFirstName2",
                "newLastName2", 222, "newCity2");
        ArrayList<Person> exp = new ArrayList<>(2);
        exp.add(exp0);
        exp.add(exp1);
        Person act0 = new Person(1111111111111111111L,"firstName1",
                "lastName1", 11, "city1");
        Person act1 = new Person(2222222222222222222L,"firstName2",
                "lastName2", 22, "city2");
        ArrayList<Person> act = new ArrayList<>(2);
        act.add(act0);
        act.add(act1);
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName1" + System.lineSeparator() +
                "newLastName1" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "newCity1" + System.lineSeparator() +
                "newFirstName2" + System.lineSeparator() +
                "newLastName2" + System.lineSeparator() +
                "222" + System.lineSeparator() +
                "newCity2" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePeople(act);
        Assert.assertEquals(exp, act);
        IOScanner.setDefaultInputStream();
    }

    @Test(expected = SwitchException.class)
    public void updatePeopleSwitch(){
        Person exp0 = new Person(1111111111111111111L,"newFirstName1",
                "newLastName1", 111, "newCity1");
        Person exp1 = new Person(2222222222222222222L,"newFirstName2",
                "newLastName2", 222, "newCity2");
        ArrayList<Person> exp = new ArrayList<>(2);
        exp.add(exp0);
        exp.add(exp1);
        Person act0 = new Person(1111111111111111111L,"firstName1",
                "lastName1", 11, "city1");
        Person act1 = new Person(2222222222222222222L,"firstName2",
                "lastName2", 22, "city2");
        ArrayList<Person> act = new ArrayList<>(2);
        act.add(act0);
        act.add(act1);
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "newFirstName1" + System.lineSeparator() +
                "newLastName1" + System.lineSeparator() +
                "111" + System.lineSeparator() +
                "switch" + System.lineSeparator() +
                "newFirstName2" + System.lineSeparator() +
                "newLastName2" + System.lineSeparator() +
                "222" + System.lineSeparator() +
                "newCity2" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        UpdatePersonDialog.updatePeople(act);
        Assert.assertEquals(exp, act);
        IOScanner.setDefaultInputStream();
    }

    @Test(expected = WrongFormatException.class)
    public void updatePersonNull(){
        UpdatePersonDialog.updatePerson(null);
    }

    @Test(expected = WrongFormatException.class)
    public void updatePeopleNull(){
        UpdatePersonDialog.updatePeople(null);
    }
}
