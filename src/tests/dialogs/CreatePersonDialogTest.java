package src.tests.dialogs;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import src.app.models.Person;
import src.app.utils.IO.IOPrinter;
import src.app.utils.IO.IOScanner;
import src.app.utils.Utils;
import src.app.utils.exceptions.SameIdException;
import src.app.utils.exceptions.SwitchException;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.structures.CacheManager;
import src.app.view.CreatePersonDialog;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class CreatePersonDialogTest {

    @After
    public void printDelimiter(){
        IOPrinter.printLine(Utils.STRING_DELIMITER);
        IOScanner.setDefaultInputStream();
    }

    @Test
    public void createPerson(){
        Person exp = new Person(1111111111111111111L,"firstName", "lastName", 11, "city");
        CacheManager cacheManager = new CacheManager();
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
            "firstName" + System.lineSeparator() +
            "lastName" + System.lineSeparator() +
            "11" + System.lineSeparator() +
            "city" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        Person act = CreatePersonDialog.createPerson(cacheManager);

        Assert.assertEquals(exp.getFirstName(), act.getFirstName());
        Assert.assertEquals(exp.getLastName(), act.getLastName());
        Assert.assertEquals(exp.getAge(), act.getAge());
        Assert.assertEquals(exp.getCity(), act.getCity());
    }

    @Test
    public void createPersonById(){
        Person exp = new Person(1111111111111111111L,"firstName", "lastName", 11, "city");
        CacheManager cacheManager = new CacheManager();
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "firstName" + System.lineSeparator() +
                "lastName" + System.lineSeparator() +
                "11" + System.lineSeparator() +
                "city" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        Person act = CreatePersonDialog.createPerson(1111111111111111111L, cacheManager);

        Assert.assertEquals(exp, act);
    }

    @Test(expected = SameIdException.class)
    public void createPersonByIdSameId(){
        Person exp = new Person(1111111111111111111L,"firstName", "lastName", 11, "city");
        CacheManager cacheManager = new CacheManager();
        cacheManager.add(exp);
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "firstName" + System.lineSeparator() +
                "lastName" + System.lineSeparator() +
                "11" + System.lineSeparator() +
                "city" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        CreatePersonDialog.createPerson(1111111111111111111L, cacheManager);
    }

    @Test(expected = SwitchException.class)
    public void createPersonByIdSwitch(){
        CacheManager cacheManager = new CacheManager();
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "firstName" + System.lineSeparator() +
                "switch" + System.lineSeparator() +
                "11" + System.lineSeparator() +
                "city" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        CreatePersonDialog.createPerson(1111111111111111111L, cacheManager);
    }

    @Test(expected = SwitchException.class)
    public void createPersonSwitch(){
        CacheManager cacheManager = new CacheManager();
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "firstName" + System.lineSeparator() +
                        "switch" + System.lineSeparator() +
                        "11" + System.lineSeparator() +
                        "city" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        CreatePersonDialog.createPerson(cacheManager);
    }

    @Test
    public void createPeopleOne(){
        Person exp0 = new Person(1111111111111111111L,"firstName", "lastName", 11, "city");
        ArrayList<Person> exp = new ArrayList<>(1);
        exp.add(exp0);
        CacheManager cacheManager = new CacheManager();
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
            "firstName" + System.lineSeparator() +
            "lastName" + System.lineSeparator() +
            "11" + System.lineSeparator() +
            "city" + System.lineSeparator() +
            "n" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        ArrayList<Person> act = CreatePersonDialog.createPeople(cacheManager);

        Assert.assertEquals(exp.get(0).getFirstName(), act.get(0).getFirstName());
        Assert.assertEquals(exp.get(0).getLastName(), act.get(0).getLastName());
        Assert.assertEquals(exp.get(0).getAge(), act.get(0).getAge());
        Assert.assertEquals(exp.get(0).getCity(), act.get(0).getCity());
    }

    @Test
    public void createPeopleTwo(){
        Person exp0 = new Person(1111111111111111111L,"firstName1", "lastName1", 11, "city1");
        Person exp1 = new Person(2222222222222222222L,"firstName2", "lastName2", 22, "city2");
        ArrayList<Person> exp = new ArrayList<>(2);
        exp.add(exp0);
        exp.add(exp1);
        CacheManager cacheManager = new CacheManager();
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "firstName1" + System.lineSeparator() +
                "lastName1" + System.lineSeparator() +
                "11" + System.lineSeparator() +
                "city1" + System.lineSeparator() +
                "y" + System.lineSeparator() +
                "firstName2" + System.lineSeparator() +
                "lastName2" + System.lineSeparator() +
                "22" + System.lineSeparator() +
                "city2" + System.lineSeparator() +
                "n" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        ArrayList<Person> act = CreatePersonDialog.createPeople(cacheManager);

        for(int i = 0; i < 2; i++){
            Assert.assertEquals(exp.get(i).getFirstName(), act.get(i).getFirstName());
            Assert.assertEquals(exp.get(i).getLastName(), act.get(i).getLastName());
            Assert.assertEquals(exp.get(i).getAge(), act.get(i).getAge());
            Assert.assertEquals(exp.get(i).getCity(), act.get(i).getCity());
        }
    }

    @Test
    public void createPeopleSwitch(){
        CacheManager cacheManager = new CacheManager();
        ByteArrayInputStream inputStream = new ByteArrayInputStream((
                "firstName" + System.lineSeparator() +
                "lastName" + System.lineSeparator() +
                "switch" + System.lineSeparator() +
                "city" + System.lineSeparator() +
                "n" + System.lineSeparator()
        ).getBytes(StandardCharsets.UTF_8));
        IOScanner.setInputStream(inputStream);
        CreatePersonDialog.createPeople(cacheManager);
    }

    @Test(expected = WrongFormatException.class)
    public void createPersonNull(){
        CreatePersonDialog.createPerson(null);
    }

    @Test(expected = WrongFormatException.class)
    public void createPersonByIdNull(){
        CreatePersonDialog.createPerson(1L, null);
    }

    @Test(expected = WrongFormatException.class)
    public void createPeopleNull(){
        CreatePersonDialog.createPeople(null);
    }

}
