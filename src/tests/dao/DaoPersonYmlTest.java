package src.tests.dao;

import org.junit.*;
import src.app.config.FDaoPerson;
import src.app.dao.IDaoPerson;
import src.app.models.Person;
import src.app.utils.FileUtils;
import src.app.utils.exceptions.NoSuchIdException;
import src.app.utils.exceptions.WrongFormatException;
import src.app.utils.exceptions.WrongPathException;
import src.app.utils.structures.CacheManager;
import src.app.utils.command.Extension;

import java.util.ArrayList;

public class DaoPersonYmlTest {

    private static IDaoPerson dao;

    private static final Person firstPerson = new Person(1111111111111111111L, "firstName1", "lastName1", 11, "city1");

    private static final Person secondPerson = new Person(2222222222222222222L, "firstName2", "lastName2", 22, "city2");

    private static final Person thirdPerson = new Person(3333333333333333333L,  "firstName3", "lastName3", 33, "city3");

    private static final Person updatedPerson = new Person(1111111111111111111L, "updatedFirstName1", "lastName1", 11, "city1");

    private static final Person wrongUpdatedPerson = new Person(4444444444444444444L, "updatedFirstName1", "lastName1", 11, "city1");

    private static final ArrayList<Person> people = new ArrayList<>(2);

    private static final ArrayList<Person> updatedPeople = new ArrayList<>(2);

    private static CacheManager cacheManagerExp = new CacheManager();

    private static CacheManager cacheManagerAct = new CacheManager();

    private static final String fileName = "test.yml";

    private static final String notExisting = "notExisting.yml";

    @BeforeClass
    public static void setUp(){
        people.add(firstPerson);
        people.add(secondPerson);
        updatedPeople.add(updatedPerson);
        updatedPeople.add(secondPerson);
    }

    @Before
    public void before(){
        FileUtils.createFile(fileName, Extension.YML);
        dao = FDaoPerson.chooseDao(fileName, Extension.YML);
    }

    @After
    public void after(){
        FileUtils.deleteFile(fileName, Extension.YML);
        cacheManagerExp = new CacheManager();
        cacheManagerAct.clear();
        cacheManagerExp.clear();
    }

    @Test
    public void readAllEmpty(){
        cacheManagerAct = dao.readAll();
        Assert.assertEquals(cacheManagerExp, cacheManagerAct);
    }

    @Test
    public void createAll(){
        dao.createAll(people);
        cacheManagerAct = dao.getCache();
        for(Person person : people){
            cacheManagerExp.add(person);
        }
        Assert.assertEquals(cacheManagerExp, cacheManagerAct);
    }

    @Test
    public void createOne(){
        dao.create(thirdPerson);
        cacheManagerAct = dao.getCache();
        cacheManagerExp.add(thirdPerson);
        Assert.assertEquals(cacheManagerExp,cacheManagerAct);
    }

    @Test(expected = WrongFormatException.class)
    public void createNull(){
        dao.create(null);
    }

    @Test(expected = WrongFormatException.class)
    public void createAllNull(){
        dao.createAll(null);
    }

    @Test
    public void readOne(){
        dao.createAll(people);
        cacheManagerExp.add(secondPerson);
        cacheManagerAct.add(dao.read(2222222222222222222L));
        Assert.assertEquals(cacheManagerExp,cacheManagerAct);
    }

    @Test(expected = NoSuchIdException.class)
    public void readNotExisting(){
        dao.read(5555555555555555555L); }

    @Test
    public void updateOne(){
        dao.createAll(people);
        cacheManagerExp.add(updatedPerson);
        cacheManagerExp.add(secondPerson);
        dao.update(updatedPerson);
        cacheManagerAct = dao.getCache();
        Assert.assertEquals(cacheManagerExp, cacheManagerAct);
    }

    @Test
    public void updateTwo(){
        dao.createAll(people);
        cacheManagerExp.add(updatedPerson);
        cacheManagerExp.add(secondPerson);
        dao.updateAll(updatedPeople);
        cacheManagerAct = dao.getCache();
        Assert.assertEquals(cacheManagerExp, cacheManagerAct);
    }

    @Test(expected = NoSuchIdException.class)
    public void updateNoSuchId(){
        dao.update(wrongUpdatedPerson);
    }

    @Test(expected = WrongFormatException.class)
    public void updateNull(){
        dao.update(null);
    }

    @Test(expected = WrongFormatException.class)
    public void updateAllNull(){
        dao.updateAll(null);
    }

    @Test
    public void deleteAll(){
        dao.createAll(people);
        cacheManagerExp = new CacheManager();
        dao.deleteAll();
        cacheManagerAct = dao.getCache();
        Assert.assertEquals(cacheManagerExp,cacheManagerAct);
    }

    @Test
    public void delete(){
        dao.createAll(people);
        cacheManagerExp.add(firstPerson);
        dao.delete(2222222222222222222L);
        cacheManagerAct = dao.getCache();
        Assert.assertEquals(cacheManagerExp, cacheManagerAct);
    }

    @Test(expected = NoSuchIdException.class)
    public void deleteNotExisting(){
        dao.delete(5555555555555555555L); }


    @Test
    public void getCache(){
        dao.createAll(people);
        cacheManagerExp.add(firstPerson);
        cacheManagerExp.add(secondPerson);
        cacheManagerAct = dao.getCache();
        Assert.assertEquals(cacheManagerExp, cacheManagerAct);
    }

    @Test(expected = WrongPathException.class)
    public void wrongFile(){
        dao = FDaoPerson.chooseDao(notExisting, Extension.YML);    }

    @Test
    public void fileName(){
        Assert.assertEquals(fileName, dao.getFileName());
    }
}
